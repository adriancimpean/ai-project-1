﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using ZedGraph;

namespace Project_1
{
    using Label = System.Windows.Forms.Label;
    public partial class form1 : Form
    {
        Label[] inputLbl = new Label[200];
        NumericUpDown[] numericUpDownsInput = new NumericUpDown[200];

        Label[] weightLbl = new Label[200];
        NumericUpDown[] numericUpDownsWeight = new NumericUpDown[200];

        decimal inputFunction;
        int numberOfInputs = 0;

        public form1()
        {
            InitializeComponent();
            inputFunctionComboBox.SelectedItem = inputFunctionComboBox.Items[0];
            activationFunctionComboBox.SelectedItem = activationFunctionComboBox.Items[0];
            numberOfInputsNumeric.Value = 1;
        }

        private void numberOfInputsNumeric_ValueChanged(object sender, EventArgs e)
        {
            numberOfInputs = (int)numberOfInputsNumeric.Value;

            flowLayoutPanel.Controls.Clear();
            for (int i = 0; i < numberOfInputs; i++)
            {

                //INPUTS
                inputLbl[i] = new Label();
                inputLbl[i].Text = "in" + i;
                inputLbl[i].Left = 20;
                inputLbl[i].Width = 30;
                inputLbl[i].Top = label1.Top + 60 + i * 30;
                flowLayoutPanel.Controls.Add(inputLbl[i]);

                numericUpDownsInput[i] = new NumericUpDown();
                numericUpDownsInput[i].Increment = 0.01M;
                numericUpDownsInput[i].DecimalPlaces = 2;
                numericUpDownsInput[i].Left = inputLbl[i].Right;
                numericUpDownsInput[i].Width = 50;
                numericUpDownsInput[i].Top = numberOfInputsNumeric.Top + 60 + i * 30;
                numericUpDownsInput[i].Minimum = -100;
                numericUpDownsInput[i].ValueChanged += new System.EventHandler(this.calculate_input);


                flowLayoutPanel.Controls.Add(numericUpDownsInput[i]);

                //WEIGHTS
                weightLbl[i] = new Label();
                weightLbl[i].Text = "w" + i;
                weightLbl[i].Left = numericUpDownsInput[i].Right + 10;
                weightLbl[i].Width = 30;
                weightLbl[i].Top = label1.Top + 60 + i * 30;
                flowLayoutPanel.Controls.Add(weightLbl[i]);

                numericUpDownsWeight[i] = new NumericUpDown();
                numericUpDownsWeight[i].Increment = 0.01M;
                numericUpDownsWeight[i].DecimalPlaces = 2;
                numericUpDownsWeight[i].Left = weightLbl[i].Right;
                numericUpDownsWeight[i].Width = 50;
                numericUpDownsWeight[i].Top = numberOfInputsNumeric.Top + 60 + i * 30;
                numericUpDownsWeight[i].Minimum = -100;
                numericUpDownsWeight[i].ValueChanged += new EventHandler(this.calculate_input);

                flowLayoutPanel.Controls.Add(numericUpDownsWeight[i]);
            }

        }

        private void calculate_input(object sender, EventArgs e)
        {
            decimal inputResult = 0.0M;

            switch (inputFunctionComboBox.SelectedItem.ToString())
            {
                case "Suma":
                    inputResult = calculateInput(InputFunction.Suma);
                    break;

                case "Produs":
                    inputResult = calculateInput(InputFunction.Produs);
                    break;

                case "Maxim":
                    inputResult = calculateInput(InputFunction.Maxim);
                    break;

                case "Minim":
                    inputResult = calculateInput(InputFunction.Minim);
                    break;
            }

            switch (activationFunctionComboBox.SelectedItem.ToString())
            {
                case "Sigmoidala":
                    activationFunctionTextBox.Text = calculateActivation(inputResult, ActivationFunction.Sigmoidala).ToString();
                    break;

                case "Treapta":
                    activationFunctionTextBox.Text = calculateActivation(inputResult, ActivationFunction.Treapta).ToString();
                    break;

                case "TangentaHiperbolica":
                    activationFunctionTextBox.Text = calculateActivation(inputResult, ActivationFunction.TangentaHiperbolica).ToString();
                    break;

                case "Semn":
                    activationFunctionTextBox.Text = calculateActivation(inputResult, ActivationFunction.Semn).ToString();
                    break;

                case "Rampa":
                    activationFunctionTextBox.Text = calculateActivation(inputResult, ActivationFunction.Rampa).ToString();
                    break;
            }
        }

        private decimal calculateInput(InputFunction inputFunction)
        {
            if (numberOfInputs == 0)
                return 0.0M;

            switch (inputFunction)
            {
                case InputFunction.Suma:
                    decimal sum = 0;
                    for (int i = 0; i < numberOfInputs; i++)
                    {
                        sum += numericUpDownsInput[i].Value * numericUpDownsWeight[i].Value;
                    }
                    inputFunctionTextBox.Text = (sum - thetaNumericUpDown.Value).ToString();

                    return sum - thetaNumericUpDown.Value;

                case InputFunction.Produs:
                    decimal product = 1;
                    for (int i = 0; i < numberOfInputs; i++)
                    {
                        product *= numericUpDownsInput[i].Value * numericUpDownsWeight[i].Value;
                    }
                    inputFunctionTextBox.Text = (product - thetaNumericUpDown.Value).ToString();

                    return product - thetaNumericUpDown.Value;

                case InputFunction.Maxim:
                    decimal max = numericUpDownsInput[0].Value * numericUpDownsWeight[0].Value;
                    for (int i = 0; i < numberOfInputs; i++)
                    {
                        if (numericUpDownsInput[i].Value * numericUpDownsWeight[i].Value > max)
                        {
                            max = numericUpDownsInput[i].Value * numericUpDownsWeight[i].Value;
                        }
                    }
                    inputFunctionTextBox.Text = (max - thetaNumericUpDown.Value).ToString();

                    return max - thetaNumericUpDown.Value;

                case InputFunction.Minim:
                    decimal min = numericUpDownsInput[0].Value * numericUpDownsWeight[0].Value;
                    for (int i = 0; i < numberOfInputs; i++)
                    {
                        if (numericUpDownsInput[i].Value * numericUpDownsWeight[i].Value < min)
                        {
                            min = numericUpDownsInput[i].Value * numericUpDownsWeight[i].Value;
                        }
                    }
                    inputFunctionTextBox.Text = (min - thetaNumericUpDown.Value).ToString();


                    return min - thetaNumericUpDown.Value;
                default:
                    return 0;
            }
        }

        private void inputFunctionComboBox_SelectedValueChanged(object sender, EventArgs e)
        {

            if(activationFunctionComboBox.SelectedItem == null)
            {
                return;
            }
            switch (inputFunctionComboBox.SelectedItem.ToString())
            {
                case "Suma":
                    inputFunction = calculateInput(InputFunction.Suma);
                    inputFunctionTextBox.Text = inputFunction.ToString();
                    break;
                case "Produs":
                    inputFunction = calculateInput(InputFunction.Produs);
                    inputFunctionTextBox.Text = inputFunction.ToString();
                    break;
                case "Maxim":
                    inputFunction = calculateInput(InputFunction.Maxim);
                    inputFunctionTextBox.Text = inputFunction.ToString();
                    break;
                case "Minim":
                    inputFunction = calculateInput(InputFunction.Minim);
                    inputFunctionTextBox.Text = inputFunction.ToString();
                    break;
            }

            switch (activationFunctionComboBox.SelectedItem.ToString())
            {
                case "Sigmoidala":
                    activationFunctionTextBox.Text = calculateActivation(inputFunction, ActivationFunction.Sigmoidala).ToString();
                    break;

                case "Treapta":
                    activationFunctionTextBox.Text = calculateActivation(inputFunction, ActivationFunction.Treapta).ToString();
                    break;

                case "TangentaHiperbolica":
                    activationFunctionTextBox.Text = calculateActivation(inputFunction, ActivationFunction.TangentaHiperbolica).ToString();
                    break;

                case "Semn":
                    activationFunctionTextBox.Text = calculateActivation(inputFunction, ActivationFunction.Semn).ToString();
                    break;

                case "Rampa":
                    activationFunctionTextBox.Text = calculateActivation(inputFunction, ActivationFunction.Rampa).ToString();
                    break;
            }
        }
    

        private decimal calculateActivation(decimal input, ActivationFunction activationFunction)
        {

            decimal inputAfterTheta = input - thetaNumericUpDown.Value;
           
            if(activationFunctionComboBox.SelectedItem == null)
            {
                return 0.0M;
            }

            switch(activationFunction)
            {
                case ActivationFunction.Sigmoidala:
                    //return (decimal)(1 / (1 + Math.Exp((double)inputAfterTheta)));
                   
                    activationFunctionTextBox.Text = ((decimal)((Math.Exp((double)aNumericUpDown.Value * (double)inputAfterTheta) - Math.Exp((double)aNumericUpDown.Value * (double)-inputAfterTheta)) / (Math.Exp((double)aNumericUpDown.Value * (double)inputAfterTheta) + Math.Exp((double)aNumericUpDown.Value * (double)-inputAfterTheta)))).ToString();
                    return (decimal)((Math.Exp((double)aNumericUpDown.Value * (double)inputAfterTheta) - Math.Exp((double)aNumericUpDown.Value * (double)-inputAfterTheta)) / (Math.Exp((double)aNumericUpDown.Value * (double)inputAfterTheta) + Math.Exp((double)aNumericUpDown.Value * (double)-inputAfterTheta)));

                case ActivationFunction.TangentaHiperbolica:

                    activationFunctionTextBox.Text = ((decimal)Math.Tanh((double)inputAfterTheta)).ToString();
                    return (decimal)Math.Tanh((double)inputAfterTheta);

                case ActivationFunction.Treapta:
                    if (inputAfterTheta < thetaNumericUpDown.Value)
                    {
                        return calculateActivation(0, ActivationFunction.Sigmoidala);
                    }
                    return calculateActivation(1, ActivationFunction.Sigmoidala);

                case ActivationFunction.Semn:
                    if (inputAfterTheta < thetaNumericUpDown.Value)
                    {
                        return calculateActivation(-1, ActivationFunction.TangentaHiperbolica);
                    }
                    return calculateActivation(1, ActivationFunction.TangentaHiperbolica);

                case ActivationFunction.Rampa:
                    if (inputAfterTheta < -aNumericUpDown.Value)
                    {
                        activationFunctionTextBox.Text = "-1";
                        return -1;
                    } else if (inputAfterTheta > aNumericUpDown.Value)
                    {
                        activationFunctionTextBox.Text = "1";
                        return 1;
                    }
                    activationFunctionTextBox.Text = (inputAfterTheta / aNumericUpDown.Value).ToString();
                    return inputAfterTheta / aNumericUpDown.Value;
                
                default:
                    return 0;
            }
        }

        private void activationFunctionComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            decimal activationFunction;
            switch (activationFunctionComboBox.SelectedItem)
            {
                case "Sigmoidala":
                    activationFunction = calculateActivation(inputFunction, ActivationFunction.Sigmoidala);
                    activationFunctionTextBox.Text = activationFunction.ToString();
                    calculateOutput(activationFunction, OutputFunction.Sigmoidala);
                    break;
               
                case "Treapta":
                    activationFunction = calculateActivation(inputFunction, ActivationFunction.Treapta);
                    activationFunctionTextBox.Text = activationFunction.ToString();
                    calculateOutput(activationFunction, OutputFunction.Sigmoidala);
                    break;
                
                case "TangentaHiperbolica":
                    activationFunction = calculateActivation(inputFunction, ActivationFunction.TangentaHiperbolica);
                    activationFunctionTextBox.Text = activationFunction.ToString();
                    calculateOutput(activationFunction, OutputFunction.TangentaHiperbolica);
                    break;
               
                case "Semn":
                    activationFunction = calculateActivation(inputFunction, ActivationFunction.Semn);
                    activationFunctionTextBox.Text = activationFunction.ToString();
                    calculateOutput(activationFunction, OutputFunction.TangentaHiperbolica);
                    break;
                
                case "Rampa":
                    activationFunction = calculateActivation(inputFunction, ActivationFunction.Rampa);
                    activationFunctionTextBox.Text = activationFunction.ToString();
                    calculateOutput(activationFunction, OutputFunction.Rampa);
                    break;
            }
        }

       private void calculateOutput(decimal input, OutputFunction outputFunction)
        {
            if (binaryCheckBox.Checked == true)
            {
                switch (outputFunction)
                {
                    case OutputFunction.Sigmoidala:
                        if (input < (decimal)0.5)
                        {
                            outputFunctionTextBox.Text = "0";
                        }
                        else
                        {
                            outputFunctionTextBox.Text = "1";
                        }
                        break;

                    case OutputFunction.TangentaHiperbolica:
                        if (input < 0)
                        {
                            outputFunctionTextBox.Text = "-1";
                        }
                        else
                        {
                            outputFunctionTextBox.Text = "1";
                        }
                        break;

                    case OutputFunction.Rampa:
                        if (input > 0)
                        {
                            outputFunctionTextBox.Text = "1";
                        }
                        else
                        {
                            outputFunctionTextBox.Text = "-1";
                        }
                        break;
                }
            } else
            {
                outputFunctionTextBox.Text = activationFunctionTextBox.Text;
            }
            generateGraph();
       }

        private void generateGraph()
        {
            zedGraphControl.GraphPane.CurveList.Clear();
            var pane = zedGraphControl.GraphPane;
            LineItem lineItem = new LineItem("Function");
            PointPairList point = new PointPairList();


            point.Add(5.0, Math.Sin(5));
            point.Add(3.0, Math.Sin(3));
            LineItem myCurve = pane.AddCurve("Function", point, Color.Red);
           
            zedGraphControl.AxisChange();
            zedGraphControl.Invalidate();
            zedGraphControl.Refresh();

        }

        enum InputFunction
        {
            Suma,
            Produs,
            Maxim,
            Minim
        }

        enum ActivationFunction
        {
            Rampa,
            Semn,
            Sigmoidala,
            TangentaHiperbolica,
            Treapta
        }

        enum OutputFunction
        {
            Sigmoidala,
            TangentaHiperbolica,
            Rampa
        }

        private void binaryCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            switch (activationFunctionComboBox.SelectedItem.ToString())
            {
                case "Sigmoidala":
                    calculateOutput(Convert.ToDecimal(activationFunctionTextBox.Text), OutputFunction.Sigmoidala);
                    break;

                case "Treapta":
                    calculateOutput(Convert.ToDecimal(activationFunctionTextBox.Text), OutputFunction.Sigmoidala);
                    break;

                case "TangentaHiperbolica":
                    calculateOutput(Convert.ToDecimal(activationFunctionTextBox.Text), OutputFunction.TangentaHiperbolica);
                    break;

                case "Semn":
                    calculateOutput(Convert.ToDecimal(activationFunctionTextBox.Text), OutputFunction.TangentaHiperbolica);
                    break;

                case "Rampa":
                    calculateOutput(Convert.ToDecimal(activationFunctionTextBox.Text), OutputFunction.Rampa);
                    break;
            }
        }
    }
}

