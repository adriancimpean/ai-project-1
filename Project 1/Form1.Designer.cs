﻿namespace Project_1
{
    partial class form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.numberOfInputsNumeric = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.inputFunctionTextBox = new System.Windows.Forms.TextBox();
            this.inputFunctionComboBox = new System.Windows.Forms.ComboBox();
            this.activationFunctionComboBox = new System.Windows.Forms.ComboBox();
            this.activationFunctionTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.outputFunctionTextBox = new System.Windows.Forms.TextBox();
            this.binaryCheckBox = new System.Windows.Forms.CheckBox();
            this.thetaNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.aNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.zedGraphControl = new ZedGraph.ZedGraphControl();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfInputsNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thetaNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Number of inputs";
            // 
            // numberOfInputsNumeric
            // 
            this.numberOfInputsNumeric.Location = new System.Drawing.Point(132, 88);
            this.numberOfInputsNumeric.Name = "numberOfInputsNumeric";
            this.numberOfInputsNumeric.Size = new System.Drawing.Size(48, 20);
            this.numberOfInputsNumeric.TabIndex = 2;
            this.numberOfInputsNumeric.ValueChanged += new System.EventHandler(this.numberOfInputsNumeric_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(436, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Input function:";
            // 
            // inputFunctionTextBox
            // 
            this.inputFunctionTextBox.Location = new System.Drawing.Point(420, 53);
            this.inputFunctionTextBox.Name = "inputFunctionTextBox";
            this.inputFunctionTextBox.ReadOnly = true;
            this.inputFunctionTextBox.Size = new System.Drawing.Size(121, 20);
            this.inputFunctionTextBox.TabIndex = 4;
            // 
            // inputFunctionComboBox
            // 
            this.inputFunctionComboBox.FormattingEnabled = true;
            this.inputFunctionComboBox.Items.AddRange(new object[] {
            "Maxim",
            "Produs",
            "Suma",
            "Minim"});
            this.inputFunctionComboBox.Location = new System.Drawing.Point(420, 79);
            this.inputFunctionComboBox.Name = "inputFunctionComboBox";
            this.inputFunctionComboBox.Size = new System.Drawing.Size(121, 21);
            this.inputFunctionComboBox.TabIndex = 5;
            this.inputFunctionComboBox.SelectedValueChanged += new System.EventHandler(this.inputFunctionComboBox_SelectedValueChanged);
            // 
            // activationFunctionComboBox
            // 
            this.activationFunctionComboBox.FormattingEnabled = true;
            this.activationFunctionComboBox.Items.AddRange(new object[] {
            "Rampa",
            "Semn",
            "Sigmoidala",
            "TangentaHiperbolica",
            "Treapta"});
            this.activationFunctionComboBox.Location = new System.Drawing.Point(610, 79);
            this.activationFunctionComboBox.Name = "activationFunctionComboBox";
            this.activationFunctionComboBox.Size = new System.Drawing.Size(121, 21);
            this.activationFunctionComboBox.TabIndex = 8;
            this.activationFunctionComboBox.SelectedValueChanged += new System.EventHandler(this.activationFunctionComboBox_SelectedValueChanged);
            // 
            // activationFunctionTextBox
            // 
            this.activationFunctionTextBox.Location = new System.Drawing.Point(610, 53);
            this.activationFunctionTextBox.Name = "activationFunctionTextBox";
            this.activationFunctionTextBox.ReadOnly = true;
            this.activationFunctionTextBox.Size = new System.Drawing.Size(121, 20);
            this.activationFunctionTextBox.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(618, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Activation function:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(829, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Output function:";
            // 
            // outputFunctionTextBox
            // 
            this.outputFunctionTextBox.Location = new System.Drawing.Point(814, 83);
            this.outputFunctionTextBox.Name = "outputFunctionTextBox";
            this.outputFunctionTextBox.ReadOnly = true;
            this.outputFunctionTextBox.Size = new System.Drawing.Size(121, 20);
            this.outputFunctionTextBox.TabIndex = 10;
            // 
            // binaryCheckBox
            // 
            this.binaryCheckBox.AutoSize = true;
            this.binaryCheckBox.Location = new System.Drawing.Point(843, 109);
            this.binaryCheckBox.Name = "binaryCheckBox";
            this.binaryCheckBox.Size = new System.Drawing.Size(55, 17);
            this.binaryCheckBox.TabIndex = 11;
            this.binaryCheckBox.Text = "Binary";
            this.binaryCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.binaryCheckBox.UseVisualStyleBackColor = true;
            this.binaryCheckBox.CheckedChanged += new System.EventHandler(this.binaryCheckBox_CheckedChanged);
            // 
            // thetaNumericUpDown
            // 
            this.thetaNumericUpDown.DecimalPlaces = 2;
            this.thetaNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.thetaNumericUpDown.Location = new System.Drawing.Point(463, 125);
            this.thetaNumericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.thetaNumericUpDown.Name = "thetaNumericUpDown";
            this.thetaNumericUpDown.Size = new System.Drawing.Size(48, 20);
            this.thetaNumericUpDown.TabIndex = 12;
            // 
            // aNumericUpDown
            // 
            this.aNumericUpDown.Location = new System.Drawing.Point(653, 125);
            this.aNumericUpDown.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.aNumericUpDown.Name = "aNumericUpDown";
            this.aNumericUpDown.Size = new System.Drawing.Size(48, 20);
            this.aNumericUpDown.TabIndex = 13;
            this.aNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(441, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "θ:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(631, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "a:";
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.AutoScroll = true;
            this.flowLayoutPanel.Location = new System.Drawing.Point(4, 127);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(218, 385);
            this.flowLayoutPanel.TabIndex = 16;
            // 
            // zedGraphControl
            // 
            this.zedGraphControl.Location = new System.Drawing.Point(323, 184);
            this.zedGraphControl.Name = "zedGraphControl";
            this.zedGraphControl.ScrollGrace = 0D;
            this.zedGraphControl.ScrollMaxX = 0D;
            this.zedGraphControl.ScrollMaxY = 0D;
            this.zedGraphControl.ScrollMaxY2 = 0D;
            this.zedGraphControl.ScrollMinX = 0D;
            this.zedGraphControl.ScrollMinY = 0D;
            this.zedGraphControl.ScrollMinY2 = 0D;
            this.zedGraphControl.Size = new System.Drawing.Size(733, 328);
            this.zedGraphControl.TabIndex = 17;
            this.zedGraphControl.UseExtendedPrintDialog = true;
            // 
            // form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1085, 524);
            this.Controls.Add(this.zedGraphControl);
            this.Controls.Add(this.flowLayoutPanel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.aNumericUpDown);
            this.Controls.Add(this.thetaNumericUpDown);
            this.Controls.Add(this.binaryCheckBox);
            this.Controls.Add(this.outputFunctionTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.activationFunctionComboBox);
            this.Controls.Add(this.activationFunctionTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.inputFunctionComboBox);
            this.Controls.Add(this.inputFunctionTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numberOfInputsNumeric);
            this.Controls.Add(this.label1);
            this.Name = "form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numberOfInputsNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thetaNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numberOfInputsNumeric;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox inputFunctionTextBox;
        private System.Windows.Forms.ComboBox inputFunctionComboBox;
        private System.Windows.Forms.ComboBox activationFunctionComboBox;
        private System.Windows.Forms.TextBox activationFunctionTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox outputFunctionTextBox;
        private System.Windows.Forms.CheckBox binaryCheckBox;
        private System.Windows.Forms.NumericUpDown thetaNumericUpDown;
        private System.Windows.Forms.NumericUpDown aNumericUpDown;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private ZedGraph.ZedGraphControl zedGraphControl;
    }
}

